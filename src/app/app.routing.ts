import { AppComponent } from './app.component';
import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '../../node_modules/@angular/compiler/src/core';
import { LoginComponent } from './Views/login/login.component';
import { HomeComponent } from './Views/home/home.component';

const router: Routes = [
    { path: '', component: LoginComponent },
    { path: 'home', component: HomeComponent },
];


export const routes: ModuleWithProviders  = RouterModule.forRoot(router);
export const appRoutes = RouterModule.forChild(router);
