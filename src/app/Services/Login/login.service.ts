import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoginCredential } from '../../Models/login-credential';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private  httpClient:  HttpClient) { }
  API_URL  =  '/api/';


  ValidateCredential(credential : LoginCredential){
    console.log(JSON.stringify(credential));
    return  this.httpClient.post(`${this.API_URL}Login/ValidateCredential` , credential);
  }

}
