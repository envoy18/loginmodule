import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../Services/Login/login.service';
import { RoleService } from '../../Services/Roles/role.service';
import { LoginCredential } from '../../Models/login-credential';
import { JsonPipe } from '@angular/common';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  constructor(private loginService : LoginService, private roleService : RoleService) { }
  
  loginModel : LoginCredential;
  public UserName : string = "";
  public PassWord : string = "";
  

  public  GetLoginCredential(){
    this.loginModel = new LoginCredential();
    this.loginModel.UserName = this.UserName;
    this.loginModel.PassWord = this.PassWord;
    this.roleService.StoreUserRoles(this.loginModel);
  }

}
