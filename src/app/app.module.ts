import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { routes, appRoutes } from './app.routing';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from  '@angular/common/http';
import { AppComponent } from './app.component';
import { LoginComponent } from './Views/login/login.component';
import { FormsModule } from '@angular/forms';
import { RoleService } from '../app/Services/Roles/role.service';
import { LoginService} from '../app/Services/Login/login.service';
import { HomeComponent } from './Views/home/home.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot([]),
    routes,
    appRoutes
  ],
  providers: [RoleService, LoginService],
  bootstrap: [AppComponent]
})
export class AppModule { }
