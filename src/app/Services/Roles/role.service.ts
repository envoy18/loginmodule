import { Injectable } from '@angular/core';
import { LoginService } from '../Login/login.service'; 
import { LoginCredential } from '../../Models/login-credential';
import { RolesModel } from '../../Models/roles-model';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class RoleService {

  constructor(private loginService : LoginService, private router : Router) { }

  StoreUserRoles(credential : LoginCredential){
    var msg = "error";
    this.loginService.ValidateCredential(credential).subscribe(function (data){
      if(data != null){
        localStorage.setItem("roles", JSON.stringify(data["roles"]));
        msg = "success";
      }
    });
  }

  GetUserRoles(param : string){
    var data = localStorage.getItem(param);
    return data;
  }

}
