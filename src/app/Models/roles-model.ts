export class RolesModel {
    id : string
    roleGroupGuid : string
    moduleGuid : string
    canRead : boolean
    canWrite : boolean
    fullAccess : boolean
}
